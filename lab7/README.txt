Christina Floristean
cf2469
Lab #7
 
Part 1:
This part of the lab is working according to the directions. I kept the
images/text as they were originally.

Part 2:
This part of the lab is also working according to the directions. In part a, I
somewhat implemented the stat() system call for when to append "/index.html".
However, if you enter /cs3157/tng, only the text of the webpage will show up,
but not the images. Part b follows the guidelines as well. The output differs
from jae's in that the key to look up is not placed in line with the request. I
had no specific function to format the logs. I just printed out the necessary
information as I went along. The two functions sendErrorResponse and
sendOKResponse are responsible for logging and also sending that information
back to the client.  
