#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <signal.h>     /* for signal() */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netdb.h>

#define MDBSTRING "/mdb-lookup"
#define MDBKEY "/mdb-lookup?key="  
#define MAXPENDING 5    /* Maximum outstanding connection requests */
  
#define KeyMax 5

static void die(const char *message)
{
     perror(message);
     exit(1);
}


static int makeClntSocket(const char *serverName, unsigned short serverPort) {
	struct hostent *he;
	char *serverIP;
	int sock;
        struct sockaddr_in serverAddr;
	 // Get server ip from server name.
	 if ((he = gethostbyname(serverName)) == NULL) {
	       die("gethoatbyname failed");
	 }
	 serverIP = inet_ntoa(*(struct in_addr *)he->h_addr);
	 // Create socket.
	 if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	 {
	         die("socket failed");
	 }
	  
	 // Construct server address.
	 memset(&serverAddr, 0, sizeof(serverAddr));
	 serverAddr.sin_family = AF_INET;
	 serverAddr.sin_addr.s_addr = inet_addr(serverIP);
	 unsigned short port = serverPort;
	 serverAddr.sin_port = htons(port);
	 // Connect.
	 if (connect(sock, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) < 0) {
	      die("connect failed");
	 }
	 return sock;
}
static int makeServSocket(unsigned short servPort) {
    int servSock;
    struct sockaddr_in servAddr;
    // Create socket for incoming connections.
    if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
         die("socket() failed");

    // Construct local address structure.
    memset(&servAddr, 0, sizeof(servAddr));   // Zero out structure
    servAddr.sin_family = AF_INET;                // Internet address family
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY); // Any incoming interface
    servAddr.sin_port = htons(servPort);      // Local port

    // Bind to the local address.
    if (bind(servSock, (struct sockaddr *)&servAddr,
            sizeof(servAddr)) < 0)
              die("bind() failed");

    // Mark the socket so it will listen for incoming connections.
    if (listen(servSock, MAXPENDING) < 0)
            die("listen() failed"); 
    return servSock;
}

// Send error report to client with error code and message. 
void sendErrorResponse(int clntSock, int errCode, char* message){
	printf("%d %s\n", errCode, message);
        char response[1024];         
        sprintf(response, "HTTP/1.0 %d %s\n\n<html><body<h1>%d %s</h1></body></html>\r\n", errCode, message, errCode, message);
	send(clntSock, response, strlen(response), 0);
}

int checkURI(char* URI) {
	//Make sure URI starts with /.
	if (URI[0] != '/'){
                return 0;
        }
   	
	// Make sure URI doesnt end with /.. 
	if(!strcmp(URI+strlen(URI)-3, "/..")){
		return 0;
	}

	// Make sure /../ is not in the URI.
	if(strstr(URI, "/../")){
		return 0;
	}

	return 1;
}

void sendOKResponse(char* requestURI, char* webRoot, int clntSock){
	
	// Create whole path.
	char path[strlen(webRoot) + strlen(requestURI)+12];
	strcpy(path, webRoot);
	strcat(path, requestURI);
        if(requestURI[strlen(requestURI)-1] == '/') {
               strcat(path, "index.html");
        }
	else{
		struct stat buff; 
		int status = stat(path, &buff);
		if(status < 0) {
			sendErrorResponse(clntSock, 404, "Not Found");
			return;
		}
		if(!S_ISREG(buff.st_mode)) {
			strcat(path, "/index.html");
		}
	}
	FILE *fp = fopen(path, "rb");
	// Unable to open file.
	if(fp == NULL){
		sendErrorResponse(clntSock, 404, "Not Found");

		return;
	}
        char response[1024];
	printf("200 OK\n");
        sprintf(response, "HTTP/1.0 200 OK\n\n");
	send(clntSock, response, strlen(response), 0);
	char buf [4096];
	int n;
	// If fread fails, partial file content will be returned. 
	while(n = fread(buf ,1, 4096, fp)) {
		send(clntSock, buf, n, 0);
	} 
	send(clntSock, "\n", 1, 0);
	fclose(fp);

}

void processLookup(int mdbSock, int clntSock, char* requestURI) {
	char response[1024];
	printf("200 OK\n");
        sprintf(response, "HTTP/1.0 200 OK\n\n");
	send(clntSock, response, strlen(response), 0);
	
	// Lookup form.
	static const char *form =
            "<html><body>"
	    "<h1>mdb-lookup</h1>\n"
            "<p>\n"
            "<form method=GET action=/mdb-lookup>\n"
            "lookup: <input type=text name=key>\n"
            "<input type=submit>\n"
            "</form>\n"
            "<p>\n"
	    "</body></html>";
	send(clntSock,form, strlen(form), 0); 
	
	// If value was looked up, get that value. 
	if (!strncmp(requestURI, MDBKEY, strlen(MDBKEY))) {
		char value[256];
		strcpy(value, requestURI + strlen(MDBKEY));
		strcat(value, "\n");
	
		// Remove + from entries with spaces.
		int i;
		for(i = 0; i < strlen(value); i++){
			if (value[i] == '+'){
				value[i] = ' ';
			}
		}
		
		printf("Word to look up: %s\n", value);		
		send(mdbSock, value, strlen(value), 0);
	
		// Wrap the socket with a FILE* so that we can read the socket using fgets.
		FILE *fd;
		if ((fd = fdopen(mdbSock, "r")) == NULL) {
	       		return;
		}

		send(clntSock, "<p><p><table border>\n", strlen("<p><p><table border>\n"), 0);
		char buf[1024]; 
		static char trtd[] = "<tr><td>"; /* White */
		static char tryellow[] = "<tr><td bgcolor=yellow>"; /* Yellow */ 
		int white = 1;

		// Create table with entries from mdb-lookup.
		while (fgets(buf, sizeof(buf), fd) && isprint(buf[0])){
			if(white){
				send(clntSock, trtd, strlen(trtd), 0);
			}
			else{	
				send(clntSock, tryellow, strlen(tryellow), 0);
			}
			white = 1-white;
			strcat(buf, "\n");
			send(clntSock, buf, strlen(buf), 0);
	    	}
		send(clntSock, "</table>\n", strlen("</table>\n"), 0);
	}

}

void handleHTTPClient(int mdbSock, int clntSock, char* ip, char* webRoot) {
	FILE *input = fdopen(clntSock, "r");
	if (input == NULL) {
		return;
	}

	char line[4096];
	if(!fgets(line, sizeof(line), input)){
		return;
	}
	if (ferror(input) ) {
		return;
	}	

	int i;
	for(i = strlen(line)-1; !isprint(line[i]); i--) {
		line[i] = '\0';	
	}
	
	// Log the request line.
	printf("%s \"%s\" ", ip, line);  

	// Interpret request and check that it is valid.	
	char *token_separators = "\t \r\n"; // tab, space, new line
        char *method = strtok(line, token_separators);
        char *requestURI = strtok(NULL, token_separators);
        char *httpVersion = strtok(NULL, token_separators);

	if(!method || !requestURI || !httpVersion) {
		sendErrorResponse(clntSock, 400, "Bad Request");
                fclose(input);
		return;
	}

	if (strcmp(method,"GET") || strcmp(httpVersion, "HTTP/1.0") && strcmp(httpVersion, "HTTP/1.1")) {
		sendErrorResponse(clntSock, 501,"Not Implemented" );
		fclose(input);
		return;
	} 

	if(!checkURI(requestURI)){
		sendErrorResponse(clntSock, 400, "Bad Request");
		fclose(input);
		return;
	}
	
	if (!strncmp(requestURI, MDBSTRING, strlen(MDBSTRING))){
		processLookup(mdbSock, clntSock, requestURI);
	}
	else{
		sendOKResponse(requestURI, webRoot, clntSock);
	}
	fclose(input);
}

int main(int argc, char *argv[]) {
	
        // Ignore SIGPIPE so that we don't terminate when we call
        // send() on a disconnected socket.
        if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
                die("signal() failed");
	}

        if ( argc != 5) {
		fprintf(stderr, "usage: %s <server_port> <web_root> < mdb-lookup-host> <mdb-lookup-port>\n>", argv[0]); 
		exit(1);
	}

	// Process input.	
	unsigned short serverPort = atoi(argv[1]);
	char* webRoot = argv[2];
	char* mdbHost = argv[3];
	unsigned short  mdbPort = atoi(argv[4]);
	
	int servSock = makeServSocket(serverPort);
	int mdbSock = makeClntSocket(mdbHost, mdbPort);


	struct sockaddr_in clntAddr;
	unsigned int clntLen;
	int clntSock;
	for(;;) {
		clntLen = sizeof(clntAddr);

		if((clntSock = accept(servSock, (struct sockaddr *) &clntAddr, &clntLen)) <0) {
			die("accept() failed");
		}
		handleHTTPClient(mdbSock, clntSock, inet_ntoa(clntAddr.sin_addr), webRoot);
	}
}
