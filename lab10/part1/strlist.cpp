#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "strlist.h"

// Constructor
StrList::StrList(){
	initList(&list);
}

// Helper function for destructor.
void StrList::clean() {
	MyString *s;
        while((s = (MyString*)::popFront(&list))){
        	delete s;
	}
}

// Destructor
StrList::~StrList(){
	clean();
}

// Add all item from list l to the list object.
void StrList::addAll(const struct List* l) {
	// Add items backwards via addFront, then
	// reverse.
	
	reverse();
	struct Node* node = l->head;
	while(node) {
		addFront(*(MyString*)node->data);
		node = node->next;
	}
	reverse();
}


// Copy Constructor
StrList::StrList(const StrList& s) {
	initList(&list);
	addAll(&s.list);

}

// Operator =
StrList& StrList::operator=(const StrList& s) {

	// Return if list is equal to s.
	if(this == &s) {
		return *this;
	}
	clean();
	addAll(&s.list);
	return *this;

	
}

// Return number of elements in the list.
int StrList::size() const {
	int counter = 0;
	struct Node* node = list.head;
	while(node) {
		counter++;
		node = node->next;
	}
	return counter;
}

// Add an element to the front of the list.
void StrList::addFront(const MyString& s) {
	MyString *string = new MyString(s);

	// Use function from C list code.
	::addFront(&list,string); 	
}

// Remove element at the front of the list.
MyString StrList::popFront() {

	// Use function from C list code.
	MyString* s = (MyString *)::popFront(&list);
	MyString string;
	if(s) {
		string = *s;	
		delete s;
	}

	return string;
}

// Reverse order of list.
void StrList:: reverse() {
	reverseList(&list);
}

// Operator +=
StrList& StrList::operator+=(const StrList& s) {
	addAll(&s.list);
	return *this;
}

// Operator +
StrList operator+(const StrList& a, const StrList& b) {
	/* StrList s;
	s.addAll(&a.list);
	s.addAll(&b.list);
	return s;*/
	
	StrList temp(a);
	temp += b;
	return temp;
}

// Operator <<
ostream& operator <<(ostream& os, const StrList& s) {
	os << "{ ";
	struct Node* node = s.list.head;
	while(node) {
		os << *(MyString*)node->data<< " ";
		node = node->next;
	}
	os << "} ";
	return os;
}

// Operator []
MyString& StrList::operator[](int i) {
	struct Node* node = list.head;
	int counter;
	for(counter = 0; node && counter < i; counter ++) {
		node = node->next;
	}
	return *(MyString*)node->data;
}
const MyString& StrList::operator[](int i) const {
	return ((StrList&)*this)[i];
}
 
