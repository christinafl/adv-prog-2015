Christina Floristean
cf2469
Lab #10

Part 1:
This part of the lab works as outlined by the directions. Private methods
clean() and addAll() were added to make the functionality of the destructor and
copy constuctor more readily available, since you cannot call those functions. 
Clean() deletes all the items from the list and addAll() appends items from another 
list to the member variable list. At this point in time, operator + is a friend. 

Part 2:
This part of the lab also works according to the directions provided. Clean()
and addAll() do not exist anymore due to the functions already available from
using the list template. Most functions were rewritten so that the old C list
code was no longer needed and only the functions of the list template were
implemented. Operator + was implemented using operator += and no longer needed
to be a friend.  

Part 3:  
This part of the lab works according to the directions provided. The deque
container was used instead of the list containter. Deque has O(1) time for the
operator []. The only function that needed to be reimplemented was reverse(),
which used list's reverse() function. This was done easily enough, and besides
this no other major changes were necessary. 
