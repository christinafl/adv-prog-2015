#ifndef __TLIST_H__
#define __TLIST_H__

#include <string>
#include <deque>
#include <iostream>
#include <algorithm>

using namespace std;

template <typename T>
class TList;
template <typename T>
ostream& operator<<(ostream& os, const TList<T>& s);

template <typename T>
TList<T> operator+(const TList<T>& a, const TList<T>& b);

template <typename T>
class TList {

	// Public functions.
	// Deque is used instead of list, so some functions
	// did not match up with list, namely reverse().  
	public:
		int isEmpty() const { return l.empty();}
		int size() const { return l.size();}
		void addFront(const T& s) { l.push_front(s);}
		T popFront();
		void reverse();
		TList<T>& operator+=(const TList<T>& s);
		friend ostream& operator<< <T>(ostream&	os, const TList<T>& s);
		T& operator[](int i);
		const T& operator[](int i) const;
	private:
		deque<T> l;
};

// Methods written using functions given by deque. Again, the old
// C list is not used anymore.

template <typename T>
T TList<T>::popFront() {
	T front = l.front();
	l.pop_front();
	return front;
}

template <typename T>
TList<T>& TList<T>::operator+=(const TList<T>& s) {
	for (auto i = s.l.begin(); i != s.l.end(); ++i) {
        	l.push_back(*i);
	}
	return *this;
}

template <typename T>
TList<T> operator +(const TList<T>& a,const TList<T>& b) {
	TList<T> temp(a);
	temp += b;
	return temp;
}

template <typename T>
void TList<T>::reverse() {
	deque<T> t;

	// Add values of deque l in reverse order to t, then
	// assign t to l.
	for(auto i = l.begin(); i!= l.end(); ++i) {
		t.push_front(*i);
	}
	l = t;
}
	
template <typename T>
ostream& operator <<(ostream& os, const TList<T>& s) {
	os << "{ ";
        for (auto j = s.l.begin(); j != s.l.end(); ++j) {
		os<< *j<< " ";
	}
	os << "} ";
	return os;
}

template <typename T>
T& TList<T>::operator[](int i) {
	int counter = 0;
	auto idx = l.begin();
        while (counter < i) {
		counter++;
		++idx;
	}	
	return *idx;
}

template <typename T>
const T& TList<T>::operator[](int i) const {
	return ((TList<T>&)*this)[i];
}
#endif
