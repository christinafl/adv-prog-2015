#ifndef __TLIST_H__
#define __TLIST_H__

#include <string>
#include <list>
#include <iostream>
#include <algorithm>

using namespace std;

template <typename T>
class TList;
template <typename T>
ostream& operator<<(ostream& os, const TList<T>& s);

template <typename T>
TList<T> operator+(const TList<T>& a, const TList<T>& b);

template <typename T>
class TList {
	
	// Public methods. 
	// Many use the functions that list already has.  
	public:
		int isEmpty() const { return l.empty();}
		int size() const { return l.size();}
		void addFront(const T& s) { l.push_front(s);}
		T popFront();
		void reverse() { l.reverse();}
		TList<T>& operator+=(const TList<T>& s);
		friend ostream& operator<< <T>(ostream&	os, const TList<T>& s);
		T& operator[](int i);
		const T& operator[](int i) const;
	private:
		list<T> l;
};

// All functions written with no reference to or use of old C list.

template <typename T>
T TList<T>::popFront() {
	T front = l.front();
	l.pop_front();
	return front;
}

template <typename T>
TList<T>& TList<T>::operator+=(const TList<T>& s) {

	// Iterator through s and append to list l.
	for (auto i = s.l.begin(); i != s.l.end(); ++i) {
        	l.push_back(*i);
	}
	return *this;
}

template <typename T>
TList<T> operator +(const TList<T>& a,const TList<T>& b) {
	TList<T> temp(a);
	temp += b;
	return temp;
}
	
template <typename T>
ostream& operator <<(ostream& os, const TList<T>& s) {
	os << "{ ";
        for (auto j = s.l.begin(); j != s.l.end(); ++j) {
		os<< *j<< " ";
	}
	os << "} ";
	return os;
}

template <typename T>
T& TList<T>::operator[](int i) {
	int counter = 0;
	auto idx = l.begin();
        while (counter < i) {
		counter++;
		++idx;
	}	
	return *idx;
}

template <typename T>
const T& TList<T>::operator[](int i) const {
	return ((TList<T>&)*this)[i];
}
#endif
