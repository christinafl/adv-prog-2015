#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

// Compares two elements so that the smaller one appears before the larger
// one in an array. 
int comparAscend(const void* a, const void* b){
	return ( *(int*)a - *(int*)b );
}

// Compares two elements so that the larger one appears before the smaller
// one in an array.
int comparDescend(const void* a, const void* b){
	return ( *(int*)b - *(int*)a );
}

// Determines which compare function to use.
void sort_integer_array(int *begin, int *end, int ascending){
	if (ascending == 1){
		qsort(begin, end - begin, sizeof(int), comparAscend);
	}
	else{
		qsort(begin, end - begin, sizeof(int), comparDescend);
	}
}

// Prints the contents of an array of integers.
void print_int_array(int *a, int size){
	int i;
	for(i = 0; i < size; i++){
		printf("%d  ", a[i]);
	}
	printf("\n\n");
}

int main(){
	int size, i, byt;
	
	// Obtain the array size and allocate enough space for three arrays.
	scanf("%d", &size);
	byt = size * sizeof(int);
	int *p = (int *) malloc(byt);
	int *q = (int *) malloc(byt);
	int *r = (int *) malloc(byt);

	if( p == NULL ||q == NULL || r ==NULL) {
		perror("malloc returned NULL");
		exit(1);
	}
	
	//Generate an array of random numbers.
	srandom(time(NULL));
	for(i = 0; i < size; i++){
		p[i] = random();
	}

	// Print original array.
	printf("Original Array:\n");
	print_int_array(p, size);
	
	// Make a copy of the array and sort the array in ascending order.
	memcpy(q, p, byt);
	sort_integer_array(q, q+size, 1);
	printf("Ascending Array:\n");
	print_int_array(q, size);
	
	// Make a copy of the array and sort it in descending order.
	memcpy(r, p, byt);
	sort_integer_array(r, r+size, 0);
	printf("Descending Array:\n");
	print_int_array(r, size);

	// Free memory.
	free(p);
	free(q);
	free(r);
	
	return 0;

}

