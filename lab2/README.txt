Christina Floristean
cf2469
Lab 2

Part 1:
This part of the lab functions as directed by the instructions of lab
2. The function qsort() was used to sort the array in ascending and 
descending order. Two compare functions (ascend/descend) were added because
one of the parameters of qsort is a pointer to such functions. A print
function was added to print the contents of the arrays.

Part 2:
This part of the lab also functions as directed by the instructions. 
In duplicateArgs(), pointer arithmetic was used to navigate through the
array. 

Valgrind output for part 1 of the lab:
==25174== Memcheck, a memory error detector
==25174== Copyright (C) 2002-2011, and GNU GPL'd, by Julian Seward et al.
==25174== Using Valgrind-3.7.0 and LibVEX; rerun with -h for copyright info
==25174== Command: ./isort
==25174== 
Original Array:
1970557873  939067920  1052523740  603385308  1857641947  

Ascending Array:
603385308  939067920  1052523740  1857641947  1970557873  

Descending Array:
1970557873  1857641947  1052523740  939067920  603385308  

==25174== 
==25174== HEAP SUMMARY:
==25174==     in use at exit: 0 bytes in 0 blocks
==25174==   total heap usage: 3 allocs, 3 frees, 60 bytes allocated
==25174== 
==25174== All heap blocks were freed -- no leaks are possible
==25174== 
==25174== For counts of detected and suppressed errors, rerun with: -v
==25174== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)

Valgrind output for part 2 of the lab:
==25382== Memcheck, a memory error detector
==25382== Copyright (C) 2002-2011, and GNU GPL'd, by Julian Seward et al.
==25382== Using Valgrind-3.7.0 and LibVEX; rerun with -h for copyright info
==25382== Command: ./twecho hello world dude
==25382== 
hello HELLO
world WORLD
dude DUDE
==25382== 
==25382== HEAP SUMMARY:
==25382==     in use at exit: 0 bytes in 0 blocks
==25382==   total heap usage: 5 allocs, 5 frees, 66 bytes allocated
==25382== 
==25382== All heap blocks were freed -- no leaks are possible
==25382== 
==25382== For counts of detected and suppressed errors, rerun with: -v
==25382== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
