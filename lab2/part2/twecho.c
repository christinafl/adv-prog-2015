/*
 * twecho
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Make a copy of the array of arguments with all the characters being
// capital letters.
static char **duplicateArgs(int argc, char **argv)
{
	char **copy;
	char **p;
	char **q;

	// Allocate appropriate amount of space.
	copy = (char **)malloc((argc+1) * sizeof(char *));
	if ( copy == NULL){
		perror("malloc returned NULL");
		exit(1);
	}
	p = argv;
	q = copy;

	while(*p){

		// Allocate space. 
		*q = (char *)malloc(strlen(*p)+1);
		if(*q == NULL){
			perror("malloc returned NULL");
			exit(1);
		}

		// Use pointers *pp and *qq to go through every character
		// and capitalize it, while leaving *p and *q to point
		// at the beginning of the string.
		char *pp = *p;
		char *qq = *q;
		while(*pp){
			*qq = toupper(*pp);
			qq++;
			pp++;
		}
		*qq = 0;
		p++;
		q++;
	}

	// Last element needs to be a NULL pointer.
	*q = NULL;
	return copy;
}

// Free memory, including individual strings and the overall array.
static void freeDuplicatedArgs(char **copy)
{
	char **p = copy;
	while(*p){
		free(*p);
		p++;
	}	
	free(copy);
}

/*
 * DO NOT MODIFY main().
 */
int main(int argc, char **argv)
{
    if (argc <= 1)
        return 1;

    char **copy = duplicateArgs(argc, argv);
    char **p = copy;

    argv++;
    p++;
    while (*argv) {
        printf("%s %s\n", *argv++, *p++);
    }

    freeDuplicatedArgs(copy);

    return 0;
}
