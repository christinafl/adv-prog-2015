#include <stdio.h>

void convertBin(unsigned int x);

/* Function to convert unsigned decimals to binary numbers */
void convertBin(unsigned int x){ 
	/* Constant = 2^31 */
	unsigned int constant = 2147483648;
	int counter = 0;	
	while(constant > 0){
		
		/* If result is all 0's, print 0 */
		if((constant & x) == 0){
			printf("0");
			counter++;
		}
		else{
			printf("1");
			counter++;
		}
		
		/* Divide constant by 2 */
		constant = constant >> 1;	
		
		/* Manage spacing */
		if(counter == 4){
			printf(" ");
			counter = 0;
		}
	}
	printf("\n");
}
	

int main()
{
	
	/* Spaces added to line up results */
	int x;    
	scanf("%d", &x);
    	printf("signed dec:    %d\n", x);
    	printf("unsigned dec:  %u\n", x);
    	printf("hex:           %x\n", x);
	printf("binary:        ");
 	convertBin((unsigned)x);
    	return 0;
}
