Christina Floristean
cf2469
Lab #1

Program Description:

Part 1 - 
All of the parts of part 1 of the homework are functional. The GCD
calculation was done using the recursive implementation found on this
website: http://introcs.cs.princeton.edu/java/23recursion/Euclid.java.html.
To determine if a number is prime or not, it was divided by all the factors
less than or equal to its square root, since any factor of the number must
be in this range. The math library was used for the square root function. 

Part 2-
All of the parts of part 2 are functional and follow the specifications
mentioned. To convert numbers from unsigned decimals to binary, bitwise
operators >> and & were used.   
  

