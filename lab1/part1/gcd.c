#include "gcd.h"

/* Recursively find the greatest common denominator of two integers */
int gcd(int x, int y){
	
	/* End condition to stop recursion */
	if (y == 0){
		return x;
	}
	else{
		return gcd(y, x%y);
	}
}
