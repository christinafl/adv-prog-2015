#include <stdio.h>
#include "prime.h"
#include "gcd.h"

float average(int x, int y);

/* Find the average of two integers and return a float */
float average(int x, int y)
{
	return (x+y)/2.0;
}

int main()
{
	int x, y, g;
	float avg;
	bool xprime, yprime;

	/* Obtain two positive integers from the user */
	scanf("%d", &x);
	scanf("%d", &y);
	
	/* Find the average of the two numbers and if they are prime */
	avg = average(x,y);
	xprime = prime(x);
	yprime = prime(y);
	printf("You typed in %d and %d\n", x, y);
	printf("The average is %f\n", avg);
	
	if(xprime){
		printf("%d is a prime number.\n", x);
	}
	else{
		printf("%d is not a prime number.\n", x);
	}
	if(yprime){
		printf("%d is a prime number.\n", y);
	}
	else{
		printf("%d is not a prime number.\n", y);
	}
	
	/* Determine if numbers are coprime or not */
	g = gcd(x,y);
	if (g == 1){
		printf("%d and %d are coprime.\n", x, y);
	}
	else{
		printf("%d and %d are not coprime.\n", x,y);
	}
	return 0;  
}

