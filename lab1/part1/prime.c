#include <math.h>
#include <stdbool.h>

/* Determine if a number is prime or not */
bool prime(int x)
{
	if(x == 1){
		return false;
	}

	else if(x == 2){
		return true;
	}
	
	/* Ignore all even cases */
	else if(x%2 == 0){
		return false;
	}

	/* Divide the integer by numbers less than or equal to its sqrt */
	else{
		bool notfound = true;
		int i = 3;
		float stop = sqrt(x);
		
		while(i <= stop && notfound){
			if(x%i == 0){
				notfound = false;
			}
			else{
				notfound = true;
			}
			i += 2;
		}
		if(notfound){
			return true;
		}
		else{
			return false;
		}
	}
}

