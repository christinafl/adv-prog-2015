#include <stdio.h>
#include <string.h>
#include "mylist.h"

static void printString(void *p){

	// Cast to char and print the string.
	printf("%s\n", (char *)p);
}

int main(int argc, char **argv){
	
	// Initialize the list. 
	struct List list;
	initList(&list);

	if(argc <= 1){
		return 1;
	}
	
	// The first argument is revecho.c, ignore it.
	argv++;
	
	// Add each argument to the list such that head points 
	// to the last argument.
	while(*argv){
		addFront(&list, *argv++);
	}
	
	// Print the strings in reverse order.
	traverseList(&list, &printString);

	// Use strcmp to determine if "dude" is in the list.
	if(findNode(&list, "dude",(int(*)(const void*, const void*))strcmp)){
		printf("\ndude found\n");
	}	
	else{
		printf("\ndude not found\n");
	}

	// Clear memory. 
	removeAllNodes(&list);
	return 0;
}
