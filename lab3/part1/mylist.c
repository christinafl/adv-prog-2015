#include <stdio.h>
#include <stdlib.h>
#include "mylist.h"

struct Node *addFront(struct List *list, void *data){
	
	// Allocate space for node.
	struct Node *n = malloc(sizeof(struct Node));
	
	if(n == NULL){
		perror("malloc did not succeed");
		return NULL;
	}

	// Make the head of the list point to this new node. 
	n->data = data;
	n->next = list->head;
	list->head = n;
	return n;
}

void traverseList(struct List *list, void (*f)(void *)){
	struct Node *cursor = list->head;
	
	// Apply the function to every node in the list.
	while (cursor){
		f(cursor->data);
		cursor = cursor->next;
	}
}

void flipSignDouble(void *data){

	// Cast to double and flip the sign.
	*((double *)data) = *(double *)data * (-1);

}

int compareDouble(const void *data1, const void *data2){
	
	// Check if the two doubles are equal.
	if (*((double *)data1) == *((double *)data2)) {
		return 0;
	}
	return 1;
}


struct Node *findNode(struct List *list, const void *dataSought, int (*compar)(const void *, const void *)){
	struct Node *cursor = list->head;
	
	// Traverse the list and determine if dataSought resides in a node. 
	while (cursor){
		if (compar(cursor->data, dataSought) == 0){
			return cursor;
		}
		cursor = cursor->next;
	}
	return NULL;
}

void *popFront(struct List *list){
	
	// Don't do anything for an empty list.
	if (list->head == 0) {
		return NULL;
	}
	
	struct Node *p = list->head;
	void *d = p->data;
	
	// Make head point to the next node in the list.
	list->head = list->head->next;
	free(p);
	return d;
}

void removeAllNodes(struct List *list){
	while(popFront(list)!= NULL){

	}
}

struct Node *addAfter(struct List *list, struct Node *prevNode, void *data){
	
	// Case where it is the first node in the list.
	if(!prevNode){
		return addFront(list, data);
	}
	struct Node *n = malloc(sizeof(struct Node));
	
	if(n == NULL) {
		perror("malloc did not succeed");
		return NULL;
	}

	// Add the node after prevNode. 
	n->data = data;
	n->next = prevNode->next;
	prevNode->next = n;
	
	return n;
	
}

void reverseList(struct List *list){
	struct Node *prv = NULL;
	struct Node *cur = list->head;
	struct Node *nxt;

	while(cur){
		
		// Traverse through the list and make the currrent 
		// node cur point to prv.
		nxt = cur->next;
		cur->next = prv;
		prv = cur;
		cur = nxt;
		
	}
	
	// Make head point to the formerly last node in the list.
	list->head = prv;
}


