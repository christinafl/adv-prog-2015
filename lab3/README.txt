Christina Floristean
cf2469
Lab 3

Part 1:
This part of the lab does what the directions ask for. In part a, the output is the same as
the test output given to us in the lab directions. In part b, the library was
successfully created and could be used in part 2 of the lab.

Part 2:
This part of the lab creates the same output as directed in the instructions. It
also follows the given guidelines. traverseList() was used to print the strings.
To find the string "dude", findNode() was used in conjunction with strcmp to
compare the strings in the nodes with "dude". The signature of strcmp was cast to the signature
of compar. The library created in part 1 was used to access the functions listed
in mylist.h.  

Valgrind for part1 of the lab:
==31456== Memcheck, a memory error detector
==31456== Copyright (C) 2002-2011, and GNU GPL'd, by Julian Seward et al.
==31456== Using Valgrind-3.7.0 and LibVEX; rerun with -h for copyright info
==31456== Command: ./mylist-test
==31456== 
testing addFront(): 9.0 8.0 7.0 6.0 5.0 4.0 3.0 2.0 1.0 
testing flipSignDouble(): -9.0 -8.0 -7.0 -6.0 -5.0 -4.0 -3.0 -2.0 -1.0 
testing flipSignDouble() again: 9.0 8.0 7.0 6.0 5.0 4.0 3.0 2.0 1.0 
testing findNode(): OK
popped 9.0, the rest is: [ 8.0 7.0 6.0 5.0 4.0 3.0 2.0 1.0 ]
popped 8.0, the rest is: [ 7.0 6.0 5.0 4.0 3.0 2.0 1.0 ]
popped 7.0, the rest is: [ 6.0 5.0 4.0 3.0 2.0 1.0 ]
popped 6.0, the rest is: [ 5.0 4.0 3.0 2.0 1.0 ]
popped 5.0, the rest is: [ 4.0 3.0 2.0 1.0 ]
popped 4.0, the rest is: [ 3.0 2.0 1.0 ]
popped 3.0, the rest is: [ 2.0 1.0 ]
popped 2.0, the rest is: [ 1.0 ]
popped 1.0, the rest is: [ ]
testing addAfter(): 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 
popped 1.0, and reversed the rest: [ 9.0 8.0 7.0 6.0 5.0 4.0 3.0 2.0 ]
popped 9.0, and reversed the rest: [ 2.0 3.0 4.0 5.0 6.0 7.0 8.0 ]
popped 2.0, and reversed the rest: [ 8.0 7.0 6.0 5.0 4.0 3.0 ]
popped 8.0, and reversed the rest: [ 3.0 4.0 5.0 6.0 7.0 ]
popped 3.0, and reversed the rest: [ 7.0 6.0 5.0 4.0 ]
popped 7.0, and reversed the rest: [ 4.0 5.0 6.0 ]
popped 4.0, and reversed the rest: [ 6.0 5.0 ]
popped 6.0, and reversed the rest: [ 5.0 ]
popped 5.0, and reversed the rest: [ ]
==31456== 
==31456== HEAP SUMMARY:
==31456==     in use at exit: 0 bytes in 0 blocks
==31456==   total heap usage: 18 allocs, 18 frees, 288 bytes allocated
==31456== 
==31456== All heap blocks were freed -- no leaks are possible
==31456== 
==31456== For counts of detected and suppressed errors, rerun with: -v
==31456== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)

Valgrind for part2 of the lab:
==4312== Memcheck, a memory error detector
==4312== Copyright (C) 2002-2011, and GNU GPL'd, by Julian Seward et al.
==4312== Using Valgrind-3.7.0 and LibVEX; rerun with -h for copyright info
==4312== Command: ./revecho hello world dude
==4312== 
dude
world
hello

dude found
==4312== 
==4312== HEAP SUMMARY:
==4312==     in use at exit: 0 bytes in 0 blocks
==4312==   total heap usage: 3 allocs, 3 frees, 48 bytes allocated
==4312== 
==4312== All heap blocks were freed -- no leaks are possible
==4312== 
==4312== For counts of detected and suppressed errors, rerun with: -v
==4312== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
