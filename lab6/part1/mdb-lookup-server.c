
/*
 * mdb-lookup-server.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>

#include "mylist.h"
#include "mdb.h"

#define KeyMax 5
#define MAXPENDING 5

static void die(const char *message)
{
    /* 
     * Report errors and quit 
     */

    perror(message);
    exit(1); 
}

int load(FILE *fp, struct List *dest) {
        /*
 	 * Read all records into memory.
 	 */
 	 
        struct MdbRec r;
        struct Node *node = NULL;
        int count = 0;
  
        while (fread(&r, sizeof(r), 1, fp) == 1) {
    
            // Allocate memory for a new record and copy into it the one
            // that was just read from the database.
            struct MdbRec *rec = (struct MdbRec *)malloc(sizeof(r));
            if (!rec)
                return -1;
            memcpy(rec, &r, sizeof(r));
    
            // Add the record to the linked list.
            node = addAfter(dest, node, rec);
            if (node == NULL)
                return -1;
    
            count++;
        }
    
        // See if fread() produced error.
        if (ferror(fp))
            return -1;
    
        return count;
}

void HandleTCPClient(int clntSocket, struct List* list) {
        /*
         * Lookup loop.
         */

	// Wrap the socket. 
        FILE *input = fdopen(clntSocket, "r");
        if(input == NULL)
	    die("Wrapping socket failed.");

	char line[1000];
        char key[KeyMax + 1];
        char buffer[1000];
        int numChars;
        int charsWrit;
    
        while (fgets(line, sizeof(line), input) != NULL) {
    
            // Must null-terminate the string manually after strncpy().
            strncpy(key, line, sizeof(key) - 1);
            key[sizeof(key) - 1] = '\0';
	    // If newline is there, remove it.
	    size_t last = strlen(key) - 1;
	    if (key[last] == '\n')
	        key[last] = '\0';
	       
	    // Traverse the list, printing out the matching
	    // records.
	    struct Node *node = list->head;
	    int recNo = 1;
	    while (node) {
	        struct MdbRec *rec = (struct MdbRec*)node->data;
	        if (strstr(rec->name, key) || strstr(rec->msg, key)) {
	            charsWrit =  sprintf(buffer, "%4d: {%s} said {%s}\n", recNo, rec->name, rec->msg);
	      	    if((numChars = send(clntSocket, buffer, charsWrit, 0)) != charsWrit)
            		die("send() failed");
		}
	        node = node->next;
	        recNo++;
	    }
	}

	// See if fgets() produced error.
	if (ferror(input)){
		die("input");
	}
	// Blank line to indicate end.
	charsWrit = sprintf(buffer, "\n");
	if((numChars = send(clntSocket, buffer, charsWrit, 0)) != charsWrit)
		die("send() failed");
     
	fclose(input);
	       
}
	      

int main(int argc, char *argv[])
{
    /*
     * Open the database file specified in the command line.
     */

    int servSock;
    int clntSock;
    struct sockaddr_in servAddr;
    struct sockaddr_in clntAddr;
    unsigned short servPort;
    unsigned int clntLen; 

    // Ignore SIGPIPE so that we don't terminate when we call
    // send() on a disconnected socket.
    if (signal(SIGPIPE,SIG_IGN) == SIG_ERR){ 
	die("signal() failed");
    }

    if (argc != 3) {
        fprintf(stderr, "usage: %s <database_file> <Server Port>", argv[0]);
        exit(1);
    }
    char *filename = argv[1]; 
    servPort = atoi(argv[2]);

    // Create socket for incoming connections.
    if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
	die("socket() failed");
    }

    // Construct local address structure.
    memset(&servAddr, 0, sizeof(servAddr));   /* Zero out structure */
    servAddr.sin_family = AF_INET;                /* Internet address family */
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    servAddr.sin_port = htons(servPort);      /* Local port */

    // Bind to the local address.
    if (bind(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0){
        die("bind() failed");
    }

    // Mark the socket so it will listen for incoming connections.
    if (listen(servSock, MAXPENDING) < 0){
        die("listen() failed");
    }

    for (;;) /* Run forever */
    {
        // Set the size of the in-out parameter.
        clntLen = sizeof(clntAddr);

        // Wait for a client to connect.
        if ((clntSock = accept(servSock, (struct sockaddr *) &clntAddr, 
                               &clntLen)) < 0){
            die("accept() failed");
        }

        // clntSock is connected to a client!

        printf("Handling client %s\n", inet_ntoa(clntAddr.sin_addr));
   
	// Read all db record into memory.
        FILE *fp = fopen(filename, "rb");
    
        if (fp == NULL)
            die(filename);
    
        struct List list;
        initList(&list);
 
	// Load the db file into memory.
        int loaded = load(fp, &list);
        if (loaded < 0)
           die("loadmdb");
    
        fclose(fp);
        
	// Handle client dialogue.
	HandleTCPClient(clntSock, &list);

	
	 // Clean up. 
	traverseList(&list, &free);
	removeAllNodes(&list);
    }
   
    /* NOT REACHED */
}
