
/*
 * http-client.c
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>

#define KeyMax 5
#define MAXPENDING 5
#define SENDBUFFSIZE 512
#define RCVBUFSIZE 1024


static void die(const char *message)
{
    /* 
     * Report error and quit. 
     */
    perror(message);
    exit(1); 
}

void createGetRequest(char* serverName, char* portNum, char* filePath, char* getString ){
	/* 
         * Construct get request string. 
         */
	sprintf(getString, "GET %s HTTP/1.0\r\nHost: %s:%s\n\r\n\r", filePath, serverName, portNum);
}

int main(int argc, char *argv[])
{
    /*
     * Open the database file specified in the command line.
     */

    int sock;                        /* Socket descriptor */
    struct sockaddr_in servAddr;     /* HTTP server address */
    unsigned short servPort;         /* HTTP server port */
    char getString[SENDBUFFSIZE] ;   /* String to send to HTTP server */
    unsigned int getStringLen;       /* Length of string to HTTP */
    
    // Ignore SIGPIPE so that we don't terminate when we call
    // send() on a disconnected socket.
    if (signal(SIGPIPE,SIG_IGN) == SIG_ERR){ 
	die("signal() failed");
    }

    if (argc != 4) {
        fprintf(stderr, "usage: %s <Server> <Port> <File Path>", argv[0]);
        exit(1);
    }
    char *serverName = argv[1];
    servPort = atoi(argv[2]);
    char *filePath = argv[3];

    // Extract file name. 
    char* fileName = NULL;
    char temp[256];
    strcpy(temp, filePath);
    char* token = strtok(temp,"/"); 
    while(token) {
	fileName = token;
       	token = strtok(NULL,"/");
    }

    if (!fileName) { 
	printf("Bad file path");
	exit(1);
    }

    // Get IP address of server.
    struct hostent *he;

    if ((he = gethostbyname(serverName)) == NULL) {
           die("gethoatbyname failed");
    }
    char *serverIP = inet_ntoa(*(struct in_addr *)he->h_addr);

    // Create a reliable, stream socket using TCP.
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        die("socket() failed");

    // Construct the server address structure.
    memset(&servAddr, 0, sizeof(servAddr));         /* Zero out structure */
    servAddr.sin_family      = AF_INET;             /* Internet address family */
    servAddr.sin_addr.s_addr = inet_addr(serverIP); /* Server IP address */
    servAddr.sin_port        = htons(servPort);     /* Server port */

    // Establish the connection to the HTTP server.
    if (connect(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
        die("connect() failed");

    createGetRequest(serverName, argv[2], filePath, getString);

    getStringLen = strlen(getString);          /* Determine input length */

    // Send the string to the server.
    if (send(sock, getString, getStringLen, 0) != getStringLen)
        die("send() sent a different number of bytes than expected");
    
    // Receive response from the server.
    FILE *input = fdopen(sock, "r");
    if(input == NULL)
           die("Wrapping socket failed.");
  
    char line[RCVBUFSIZE];
  
    // Determine if request was successful.
    if (fgets(line, sizeof(line), input) != NULL){
	strcpy(temp, line);
	token = strtok(temp, " ");
	
	while((token = strtok(NULL, " "))!= NULL) {
	    if(token[0] != '\0'){
		break;
	    }
	} 
	if (token == NULL || atoi(token) != 200) {
            printf("%s\n", line);
	    exit(1); 
        }
    }

    // See if fgets() produced error.
    if (ferror(input)){
         die("input");
    }

    // Jump to start of HTML file content.
    while (fgets(line, sizeof(line), input) != NULL) {
	if (!isprint(line[0])) {
		break;
	}
	
    }

    if (ferror(input)){
         die("input");
    }

    // Save content to file in directory. 
    FILE *htmlFile = fopen(fileName, "w"); 
    while (fgets(line, sizeof(line), input) != NULL) {
	fprintf(htmlFile, "%s", line);    
    }

    if (ferror(input)){
         die("input");
    }

    // Clean up and close everything.
    fclose(htmlFile);
    fclose(input);
    close(sock);
    exit(0);
}
