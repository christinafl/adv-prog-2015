Christina Floristean
cf2469
Lab #6

Program Description:

Part1:
The program works as outlined by the directions. As suggested, I combined
TCPEchoServer.c with mdb-lookup.c to this this part of the lab. 

Part2:
The program works as outlined by the directions. I used TCPEchoClient.c from the
same website to structure the program. I used fdopen() to wrap the socket into a
file pointer to read it line by line, instead of the byte array used in
TCPEchoClient.c. I tested the functionality of this program by using the diff
command to compare it with the output of wget. Strtok() was used to isolate the
name of the file out of the path and also to isolate the http response code
token. 
