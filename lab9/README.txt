Christina Floristean
cf2469
Lab #9

Description: 

Answers for part 1 of the lab:
a)
BASIC4TRACE: (0x7fff88c56f70)->MyString(const char *): 13, constructor, x
BASIC4TRACE: (0x7fff88c56f80)->MyString(const char *): 14, constructor, y
BASIC4TRACE: (0x7fff88c56fb0)->MyString(const MyString&):16, copy constructor, a
BASIC4TRACE: (0x7fff88c56fa0)->MyString(const MyString&):16, copy constructor, b
BASIC4TRACE: (0x7fff88c56f20)->MyString(const char *): 7, constructor, t
BASIC4TRACE: op+(const MyString&, const MyString&): entering operator +
BASIC4TRACE: (0x7fff88c56ed0)->MyString(): 8, constructor, temp
BASIC4TRACE: (0x7fff88c56f30)->MyString(const MyString&): 8, copy constructor,
u1 from return temp

BASIC4TRACE: (0x7fff88c56ed0)->~MyString(): 8, destructor, temp
BASIC4TRACE: op+(const MyString&, const MyString&): entering operator +
BASIC4TRACE: (0x7fff88c56ed0)->MyString(): 8, constructor, temp
BASIC4TRACE: (0x7fff88c56f40)->MyString(const MyString&): 8, copy constructor,
u2 from return temp

BASIC4TRACE: (0x7fff88c56ed0)->~MyString(): 8, destructor, temp
BASIC4TRACE: (0x7fff88c56fc0)->MyString(const MyString&): 8, copy constructor,
u3 from return u2

BASIC4TRACE: (0x7fff88c56f40)->~MyString(): 8, destructor, u2
BASIC4TRACE: (0x7fff88c56f30)->~MyString(): 8, destructor, u1
BASIC4TRACE: (0x7fff88c56f20)->~MyString(): 8, destructor, t
BASIC4TRACE: (0x7fff88c56f90)->MyString(const MyString&):16, copy constructor, z
BASIC4TRACE: (0x7fff88c56fc0)->~MyString(): 16, destructor, u3
BASIC4TRACE: (0x7fff88c56fa0)->~MyString(): 16, destructor, b
BASIC4TRACE: (0x7fff88c56fb0)->~MyString(): 16, destructor, a
one and two				  : cout << z << endl
BASIC4TRACE: (0x7fff88c56f90)->~MyString(): 19, destructor, z
BASIC4TRACE: (0x7fff88c56f80)->~MyString(): 19, destructor, y
BASIC4TRACE: (0x7fff88c56f70)->~MyString(): 19, destructor, x

b)
X and y are now being passed by reference. Therefore, the copy constructor is
not needed to construct a and b, since they are only references. The rest of the function
and program performs the same way. 

c)
This flag does not allow a temporary to be created only to intialize another
object of the same type. In this case, temp holds a+t at first, and
then gets passed in again to get a+t+b. The compiler equates z with this second
temp. Therefore, the variables that get destructed are x, y, t, temp1, and temp2
(z).

Part 2:
This part of the lab works as outlined by the instructions. In test5.cpp, two
tests were performed per operation (< > == != <= >=). The first test was always
two MyString objects, while the second test was one MyString object and one
string literal. The string literal alternated from being on the left or
right side of the equal sign for every other operation. This was to show that
the operators could be invoked with the string literal on either side. To test
the += operator function, the two tests from the instructions were used. 
