#include "mystring.h"
#include <cassert>   


  
int main()
{
	cout<<"Testing operations for MyString objects and string literals:\n" << endl;

	MyString x("one");
	MyString x1("one");
	MyString y("two");
	MyString z("three");

	cout<<"\nTesting ==" << endl;
	// Two MyString objects.
	assert(x == x1);
 	cout<< x << " is equal to " << x1 << endl;
	// MyString object and string literal.
	assert(y == "two");
	cout<< y << " is equal to " << "two\n\n" << endl;

	cout<<"Testing !=" << endl;
	// Two MyString objects
	assert(x != y);
 	cout<< x << " is not equal to " << y << endl;
	// String literal and MyString object.
	assert("hi" != y);
 	cout<< "hi" << " is not equal to " << y << "\n\n" << endl;

	cout<<"Testing <" << endl;
	// Two MyString objects.
	assert(x < z);
 	cout<< x << " is less than " << z << endl;
	// MyString object and string literal.
	assert(z < "world");
	cout<< z << " is less than " << "world" << "\n\n "  <<endl;
	
	cout<<"Testing >" << endl;
	// Two MyString objects.
	assert(z > x);
	cout<< z << " is greater than " << x << endl;
	// String literal and MyString object.
	assert("world" > z);
	cout<< "world" << " is greater than " << z << "\n\n" <<endl;
	
	cout<<"Testing <=" << endl;
	// Two MyString objects.
	assert(x <= x1);
	cout<< x << " is less than or equal to " << x1 << endl;
	// String literal and MyString object.
	assert("hi" <= z);
	cout<< "hi" << " is less than or equal to " << z << "\n\n" << endl;

	cout<<"Testing >=" << endl;
	// Two MyString objects.
	assert(y >= x);
	cout<< y << " is greater than or equal to " << x << endl;
	// MyString object and string literal.
	assert(z >= "helloworld");
	cout<< z << " is greater than or equal to " << "helloworld"<< "\n\n" << endl;
	
	// Test for += function.
        MyString s("hello");
        s += " world";
        cout << s << "\n\n" << endl;

	
        // Test op+=() and op+().
        MyString sp(" ");
        MyString period(".");
        MyString str;
        str += "This" + sp + "should" + sp
            += "work" + sp + "without"
            += sp + "any" + sp + "memory"
            += sp + "leak"
            += period;
        cout << "\n" <<  str << "\n\n" << endl;
 
	cout<< "---------------End of testing--------------- " << endl;
 	return 0;
}
                                                                                                                    
        
