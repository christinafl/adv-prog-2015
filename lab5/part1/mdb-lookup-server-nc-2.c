
/*
 * mdb-lookup-server-nc-2.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

static void die(const char *s)
{
    perror(s);
    exit(1);
}

int main(int argc, char **argv)
{
    char input[6];
    char portNum[6];
    int i;
   
    while(1) {
	pid_t pid;
	
	// Determine if any children were terminated and list their
	// PIDS.
	while((pid = waitpid( (pid_t) -1, NULL, WNOHANG)) > 0){
		fprintf(stderr, "Child with pid %d was terminated\n", (int)pid);
	}

	printf("port number: ");
	fgets(input, 6, stdin);
	
	// Loop through user entry and remove any non-printable characters.
	for(i = strlen(input)-1; i >=0; i--) {
		if(!isprint(input[i])){
			input[i] = '\0';
		}
	}
	
	// Copy into separate char[].
	strncpy(portNum, input, 5);

	// If the user pressed enter, do not execute the following code
	// and prompt them again.
	if(portNum[0] != '\0') {
		pid = fork();		
        	if (pid < 0) {
	    	die("fork failed");

        	} else if (pid == 0) {
	    	// child process
	    	execl("./mdb-lookup-server-nc.sh", "mdb-lookup-server-nc.sh", 
	 	    	portNum, (char *)0);
	    	die("execl failed");
        	} else {
	    	// parent process
	    	fprintf(stderr, "mdb-lookup-server started on port %s with pid %d\n", portNum, (int)pid);
       		} 
	}

    }

    return 0;
}

