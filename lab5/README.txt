Christina Floristean
cf2469	
Lab #5

Part 1:
Part 1a is functional and follows the directions in the assignment. I included
the optional Ctrl-C code fragment given to us in the instructions. The results
of part 1b are shown below:  

    1   916   916   916 ?           -1 Ss       0   0:23 /usr/sbin/sshd -D
  916 25659 25659 25659 ?           -1 Ss       0   0:00  \_ sshd: cf2469 [priv] 
25659 25664 25659 25659 ?           -1 S    15997   0:00  |   \_ sshd: cf2469@pts/47 
25664 25665 25665 25665 pts/47    2249 Ss   15997   0:00  |       \_ -bash
25665  2249  2249 25665 pts/47    2249 S+   15997   0:00  |           \_ ./a.out 2469
 2249  2250  2249 25665 pts/47    2249 S+   15997   0:00  |               \_ /bin/sh ./mdb-lookup-server-nc.sh 2469
 2250  2252  2249 25665 pts/47    2249 S+   15997   0:00  |                   \_ cat mypipe-2250
 2250  2253  2249 25665 pts/47    2249 S+   15997   0:00  |                   \_ nc -l 2469
 2250  2254  2249 25665 pts/47    2249 S+   15997   0:00  |                   \_ /bin/sh /home/jae/cs3157-pub/bin/mdb-lookup-cs3157 
 2254  2255  2249 25665 pts/47    2249 S+   15997   0:00  |                       \_ /home/jae/cs3157-pub/bin/mdb-lookup /home/jae/cs3157-pub/bin/mdb-cs3157

Shell Scripts:
/bin/sh ./mdb-lookup-server-nc.sh 2469

Part 1c of the lab also follows all the directions and is functional. Since the
largest port size is five digits, I make the character arrays of size 6 ('\0' at
the end)and copied 5 printable characters from the user input into a separate character array. 
