Christina Floristean
cf2469
Lab #4

Part1:
The program follows the given directions. I decided to divide the functions such
that loadmdb and freemdb were in mdb.c, while main and matchString were in
mdb-lookup.c. I did this beause freemdb and loadmdb could be used for purposes
besides this specific program, unlike main and matchstring. matchString was
added to the mdb-lookup.c file to print out any name-message combinations that
matched the user's input. I added cf2469 as the name and orange juice as the
message for the class database.


Valgrind output for Part1:
 
==23717== Memcheck, a memory error detector
==23717== Copyright (C) 2002-2011, and GNU GPL'd, by Julian Seward et al.
==23717== Using Valgrind-3.7.0 and LibVEX; rerun with -h for copyright info
==23717== Command: ./mdb-lookup my-mdb
==23717== 
lookup: o    
    1: {cf2469} said {orange juice }
    2: {tina} said {hello world}
    3: {luna} said {pretty much done}

lookup: tina    
    2: {tina} said {hello world}

lookup: orange     
    1: {cf2469} said {orange juice }

lookup: helloooooo     
    2: {tina} said {hello world}

lookup:    
    1: {cf2469} said {orange juice }
    2: {tina} said {hello world}
    3: {luna} said {pretty much done}
 
==23717== HEAP SUMMARY:
==23717==     in use at exit: 0 bytes in 0 blocks
==23717==   total heap usage: 8 allocs, 8 frees, 776 bytes allocated
==23717== 
==23717== All heap blocks were freed -- no leaks are possible
==23717== 
==23717== For counts of detected and suppressed errors, rerun with: -v
==23717== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
