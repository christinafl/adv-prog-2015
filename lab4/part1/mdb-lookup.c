#include "mdb.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

// Counts entry number. 
int counter = 0;

char str[6];

// Finds if the entered string matched anything in the database.
void  matchString(void* r) {
	counter ++;
	struct MdbRec* rec  = (struct MdbRec*) r;
	
	// Compare the entered string with the messages and names.
	char *s_msg = strstr(rec->msg, str);
	char *s_name = strstr(rec->name,str);

	// If they're found or if the string is empty, the statement will print.
	if (s_msg != NULL || s_name != NULL) {
		printf("%4d: {%s} said {%s}\n", counter, rec->name, rec->msg);			
	}

}

int main(int argc, char **argv) {
	
	// Initialize the list.
	struct List list;
	initList(&list);
	
	// End program if not name entered.
	if (argc < 2){
		printf("Database filename not provided.");
		return 1;
	}
	
	// Open file and load it's contents into the list.
	FILE *f = fopen(argv[1], "rb");
	
	if ( f == NULL){
		printf("Cannot open file. ");
		return 1;
	}

	int num_rec = loadmdb(f, &list);
	
	// End program.
	if (num_rec <= 0){
		printf("Database empty or error reading file.");
		freemdb(&list);
		fclose(f);
		return 1;
	}
	
	char buff[1000];
	int i;
	
	printf("lookup: ");

	// Loop through user entries.
	while(fgets(buff, 1000, stdin) != NULL) {
	
		// Remove any non printable characters.
		for(i = strlen(buff)-1; i >= 0; i--) {
			if(!isprint(buff[i])){
				buff[i] = '\0';
			}			
		}
		
		strncpy(str, buff, 5);
		
		counter = 0;
		
		// Check eac string against the data entered. 
		traverseList(&list, &matchString);
		printf("\nlookup: ");
	}	

	// Free the memory.
	freemdb(&list); 
	fclose(f);
	return 0;
}
