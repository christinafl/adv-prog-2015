#include "mdb.h"
#include <stdlib.h>
#include <string.h>

// Puts data into memory.
int loadmdb(FILE *fp, struct List *dest){
	int c = 0;
	struct MdbRec *cur = (struct MdbRec*)malloc(sizeof(struct MdbRec));

	if(cur == NULL) {
		perror("malloc failed");
		exit(1);
	}
	
	// Node used as pointer to move along list. 
	struct Node *p = NULL;
	
	// Read until there is no more data in the file.
	while(fread(cur , sizeof(struct MdbRec), 1, fp) == 1) {
		
		// Add node to the front of the list.
		if(p == NULL) {
			p = addFront(dest, (void*)cur);
		}
		else{
			p = addAfter(dest, p, (void*)cur);
		}
		
		// Allocate memory again to overwrite what was previously in
		// cur.
		cur = (struct MdbRec*)malloc(sizeof(struct MdbRec));
		
		if(cur == NULL) {
			perror("malloc failed");
			exit(1);
		}

		c++;	
	}
	
	// Free memory.
	free(cur);
	
	if(ferror(fp)){
		return -1;
	}
	
	// Return number of records kept.
	return c;
}

// Free all memeory used in the list.
void freemdb(struct List *list) {
    	while(list->head != NULL){
		free(popFront(list));		
	}
}

